#include "nlaunch.h"
#include <ncurses.h>
#include <iostream>

int main(int argc, char ** argv){

	NLaunch launcher;

	launcher.run();

	return 0;
}
