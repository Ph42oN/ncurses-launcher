#ifndef UI_H
#define UI_H

#include <ncurses.h>
#include <string>
#include <vector>
#include <array>
#include "gameconfig.h"

#define MENUSIZE 6

enum current_selection {
    GAMELIST,
    MENU
};

class UI {
public:
    UI();
    ~UI();

    void refreshMain();
    void selUp();
    void selDown();
    bool back();
    int enter();
	int readKey();
	WINDOW *getConfWin();
	void touchMain();
	void editString(std::string *str);
	void selectFrom(std::vector<std::string> items, std::string *str);
	int showMessage(std::string msg, bool getkey);

	int sel[2];
	enum current_selection current;
	std::vector<Game> *games;

private:
    WINDOW *screen, *gamelist, *menuwin, *confwin;
    int menusize;
};
#endif
