#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <string.h>
#include "gameconfig.h"
#include "ui.h"
#include <algorithm>
#include <filesystem>
namespace fs = std::filesystem;

Game::Game() {
	directory="";
	exec="";
	name="";
	runner="Wine";
	runnerver="";
	winepfx="";
	dxvk="none";
	vkd3d="none";
	running=false;
	log=false;
}

GameConfig::GameConfig()
{

	confpath = (std::string)getenv("HOME")+CONFIG_PATH;
	defaultconf = loadGame("default");
	defaultconf.name = "default";
}
GameConfig::~GameConfig()
{
}

bool GameConfig::Config(Game *confgame) {
   	WINDOW *confwin = ui->getConfWin();
    Game game = *confgame;

	int key=0, sel=0;
	bool changed=TRUE;
	std::vector<std::string> conf_items;

	while(1) {
		if(changed) {
			wclear(confwin);
			wattroff(confwin, COLOR_PAIR(1));
			box(confwin, 0, 0);
			conf_items.clear();
			conf_items.emplace_back("Name:       " + confgame->name);
			conf_items.emplace_back("Directory:  " + confgame->directory);
			conf_items.emplace_back("Executable: " + confgame->exec);
			conf_items.emplace_back("Arguments:  " + confgame->args);
			conf_items.emplace_back("Runner:     " + confgame->runner);
			if (!confgame->runner.compare("Wine")) {
			    conf_items.emplace_back("Wine:       " + confgame->runnerver);
			} else {
			    conf_items.emplace_back("Proton:     " + confgame->runnerver);
			}
			conf_items.emplace_back("Wineprefix: " + confgame->winepfx);
			conf_items.emplace_back("umu id:     " + confgame->umu_id);
			conf_items.emplace_back("Cmd prefix: " + confgame->cmdpfx);
			conf_items.emplace_back("DXVK:       " + confgame->dxvk);
			conf_items.emplace_back("VKD3D:      " + confgame->vkd3d);
			conf_items.emplace_back("Env vars:   " + confgame->env);
			conf_items.emplace_back((std::string)"Game log:   " + (confgame->log ? "Enabled" : "Disabled"));
			conf_items.emplace_back("Log path:   " + confgame->logpath);
			conf_items.emplace_back("Save");
			changed=FALSE;
		}
		for (int i=0; i<conf_items.size(); i++) {
			if(i==sel) wattron(confwin, COLOR_PAIR(1));
			else wattroff(confwin, COLOR_PAIR(1));
			mvwprintw(confwin, 1+i, 1, " %s ", conf_items.at(i).c_str());
		}
		wrefresh(confwin);
		key=ui->readKey();

		std::string *temp;
		switch (key) {
		case KEY_UP:
			if (sel > 0) sel--;
			else sel=conf_items.size()-1;
			break;
		case KEY_DOWN:
			if (sel < conf_items.size()-1) sel++;
			else sel=0;
			break;
		case 10:
			switch (sel) {
				case 0:
					if (confgame->name!="default")  {
						ui->editString(&confgame->name);
					}
					break;
				case 1:	ui->editString(&confgame->directory); break;
				case 2:	ui->editString(&confgame->exec); break;
				case 3:	ui->editString(&confgame->args); break;
				case 4:	ui->selectFrom({"Wine", "Proton"}, &confgame->runner); break;
				case 5: selectRunner(confgame);	break;
				case 6: ui->editString(&confgame->winepfx); break;
				case 7: ui->editString(&confgame->umu_id); break;
				case 8: ui->editString(&confgame->cmdpfx); break;
				case 9:
					dxvkver.clear();
					dxvkver.emplace_back("none");
					readDir(&dxvkfolder, &dxvkver);
					ui->selectFrom(dxvkver, &confgame->dxvk);
					break;
				case 10:
					vkd3dver.clear();
					vkd3dver.emplace_back("none");
					readDir(&vkd3dfolder, &vkd3dver);
					ui->selectFrom(vkd3dver, &confgame->vkd3d);
					break;
				case 11: ui->editString(&confgame->env); break;
				case 12: confgame->log = !confgame->log; break;
				case 13: ui->editString(&confgame->logpath); break;
				case 14: saveGame(confgame); ui->touchMain(); return true;
			}
			changed=TRUE;
			break;
		case 27: //esc
		case 113: //Q
			key = ui->showMessage("Go back without saving? (y/enter = yes)", true);
			if (key==10 || key==121) {
				ui->touchMain();
				return false;
			}
			touchwin(confwin);
			break;
		case 115: //S
			saveGame(confgame);
			ui->touchMain();
			return true;
		}
	}
}

void GameConfig::selectRunner(Game *game) {
	std::string path;
	if (!game->runner.compare("Wine")) {
		readDir(&winefolder, &winever);
		ui->selectFrom(winever, &game->runnerver);
	} else {
		readDir(&protonfolder, &protonver);
		ui->selectFrom(protonver, &game->runnerver);
		int ret = system("umu-run --help &>/dev/null");
		if(ret != 0) {
			ui->showMessage("umu-run not found in $PATH, proton will not work", true);
		}
	}

}
void GameConfig::readDir(std::string *path, std::vector<std::string> *dirs) {
	//dirs->clear();
	if (path->empty()) {
		ui->showMessage("path is empty\n", true);
		return;
	}
	if (!fs::is_directory(*path)) {
		fs::create_directory(*path);
	}
	for (const auto & dir : fs::directory_iterator(*path)) {
		if(dir.is_directory()) {
			dirs->push_back(dir.path().filename());
		}
	}
	auto iter=dirs->begin();
	if (dirs->at(0) == "none") iter++;
	std::sort(iter, dirs->end());
}

void GameConfig::saveGame(Game *game) {
	std::string filename = confpath;
	filename += "games/"+game->name+".conf";
	//std::cout << "saving to " << filename << std::endl;
	std::ofstream gamefile(filename);
	if (!gamefile.good()) {
		ui->showMessage("Error opening file "+filename, true);
		return;
	}
	gamefile << "directory=" << game->directory << std::endl;
	gamefile << "exec=" << game->exec  << std::endl;
	gamefile << "args=" << game->args  << std::endl;
	gamefile << "runner=" << game->runner  << std::endl;
	gamefile << "runnerver=" << game->runnerver  << std::endl;
	gamefile << "umu_id=" << game->umu_id  << std::endl;
	gamefile << "winepfx=" << game->winepfx  << std::endl;
	gamefile << "cmdpfx=" << game->cmdpfx  << std::endl;
	gamefile << "dxvk=" << game->dxvk  << std::endl;
	gamefile << "vkd3d=" << game->vkd3d  << std::endl;
	gamefile << "environment:" << game->env << std::endl;
	gamefile << "log=" << (game->log ? "true" : "false") << std::endl;
	gamefile << "logpath=" << game->logpath << std::endl;
	gamefile.close();
}

Game GameConfig::loadGame(std::string name) {
	Game game;
	std::string filename = confpath;
	filename += "games/"+name+".conf";
	//std::cout << "loading " << filename << std::endl;
	std::ifstream gamefile(filename);
	if (!gamefile.good()) {
		//ui->showMessage("Error opening file: " + filename, false);
		std::cerr << "Error opening file: " << filename  << std::endl;
		return game;
	}
	game.name = name;

	std::string buffer;
	while (getline(gamefile, buffer)) {
		if(buffer.substr(0,9) == "directory") {
			game.directory = buffer.substr(buffer.find('=')+1, buffer.npos);
		} else if(buffer.substr(0,4) == "exec") {
			game.exec = buffer.substr(buffer.find('=')+1, buffer.npos);
		} else if(buffer.substr(0,4) == "args") {
			game.args = buffer.substr(buffer.find('=')+1, buffer.npos).c_str();
		} else if(buffer.substr(0,9) == "runnerver") {
			game.runnerver = buffer.substr(buffer.find('=')+1, buffer.npos).c_str();
		} else if(buffer.substr(0,6) == "runner") {
			game.runner = buffer.substr(buffer.find('=')+1, buffer.npos).c_str();
		} else if(buffer.substr(0,6) == "umu_id") {
			game.umu_id = buffer.substr(buffer.find('=')+1, buffer.npos).c_str();
		} else if(buffer.substr(0,7) == "winepfx") {
			game.winepfx = buffer.substr(buffer.find('=')+1, buffer.npos).c_str();
		} else if(buffer.substr(0,6) == "cmdpfx") {
			game.cmdpfx = buffer.substr(buffer.find('=')+1, buffer.npos).c_str();
		} else if(buffer.substr(0,4) == "dxvk") {
			game.dxvk = buffer.substr(buffer.find('=')+1, buffer.npos).c_str();
		} else if(buffer.substr(0,5) == "vkd3d") {
			game.vkd3d = buffer.substr(buffer.find('=')+1, buffer.npos).c_str();
		} else if(buffer.substr(0,12) == "environment:") {
			game.env = buffer.substr(buffer.find(':')+1, buffer.npos).c_str();
		} else if(buffer.substr(0,4) == "log=") {
			buffer = buffer.substr(buffer.find('=')+1, buffer.npos);
			if (!strcasecmp(buffer.c_str(),"true")) {
				game.log=true;
			}
		} else if (buffer.substr(0,7) == "logpath") {
			game.logpath = buffer.substr(buffer.find('=')+1, buffer.npos);
		}
	}
	gamefile.close();

	return game;
}

void GameConfig::loadGames(std::vector<Game> *games) {
	Game game;
	std::vector<fs::path> files;

	for (const auto & file : fs::directory_iterator(confpath+"games")) {
		if (file.is_regular_file() &&
			file.path().extension() == ".conf" &&
			file.path().stem() != "default") {
			files.push_back(file.path());
		}
	}
	std::sort(files.begin(), files.end());;
	for (auto &file : files) {
		game = loadGame(file.stem());
		games->emplace_back(game);
	}
}

void GameConfig::savePlayed(std::vector<Game> *games) {
	std::string filename = confpath+"time";/*
	QTime time(0,0,0);

	std::ofstream timefile(filename);
	if (!timefile.is_open()) {
		std::cerr << "Error saving timefile" << std::endl;
		return;
	}
	for(size_t i=0; i<games->size(); i++) {
		timefile << games->at(i).name << ": " << games->at(i).playhour << " "
				 << games->at(i).playmin << " " << games->at(i).playsec << std::endl;
	}
	timefile.close();*/
}
void GameConfig::loadPlayed(std::vector<Game> *games) {
	std::string name, filename = confpath+"time";
	int hour = 0,min = 0, sec = 0;

	std::ifstream timefile(filename);
	if (!timefile.is_open()) {
		//ui->showMessage("Error loading timefile", false);
		return;
	}
	std::string buffer;
	while (getline(timefile, buffer)) {
		for(size_t i=0; i<games->size(); i++) {
			int pos=buffer.find(':');
			name = buffer.substr(0, pos);
			sscanf(buffer.substr(pos+1, buffer.npos).c_str(), "%d %d %d", &hour, &min, &sec);
			//seconds = stoi(buffer.substr(pos+1, buffer.npos));
			if(name == games->at(i).name) {
				games->at(i).playhour = hour;
				games->at(i).playmin = min;
				games->at(i).playsec = sec;
				//std::cout << games->at(i).name << ": " << hour << " hours " << min << " minutes " << sec << " seconds\n";
			}
		}
	}
	timefile.close();
}
/*
void GameConfig::on_ulwglSearch_clicked()
{
	QDesktopServices::openUrl(QUrl("https://ulwgl.openwinecomponents.org/index.php"));
}*/
