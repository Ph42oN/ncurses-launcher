#ifndef LAUNCHERCONFIG_H
#define LAUNCHERCONFIG_H

#include "gameconfig.h"

struct nextProcess {
	std::string cmd;
	std::string args;
};

#pragma once
class NLaunch;

class LauncherConfig
{
public:
	LauncherConfig();
	~LauncherConfig();

	std::string confpath;
	GameConfig *gameconf;
	NLaunch *launcher;
	struct nextProcess nextProcess;

	void init();
	void config();
	void loadConfig();
	void saveConfig();
	void checkDir(std::string path);
	void downloadfile(char *url);
	void checkulwgl(std::string ulwglfolder, std::string confpath);
	void runProcess(std::string process, std::string args);
/*
private:
*/
};

#endif // LAUNCHERCONFIG_H
