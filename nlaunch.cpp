#include "nlaunch.h"
#include "gameconfig.h"
#include "ui.h"
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <ostream>
#include <sched.h>
#include <signal.h>
#include <string>
#include <sys/types.h>
#include <thread>
#include <unistd.h>
#include <vector>
namespace fs = std::filesystem;

NLaunch::NLaunch() {
	launchconf.gameconf = &gameconf;
	launchconf.launcher = this;
	launchconf.init();

	gameconf.ui = &ui;
	gameconf.loadGames(&games);
	gameconf.loadPlayed(&games);

	ui.sel[0] = 0;
	if (games.empty()) {
		ui.current = MENU;
		ui.sel[1] = 0;
	} else {
		ui.current = GAMELIST;
		ui.sel[1] = 2;
	}
	ui.games = &games;
}

NLaunch::~NLaunch() {
}

void NLaunch::run() {
	int key;

	ui.refreshMain();

	while (1) {
		key = ui.readKey();
		switch (key) {
		case KEY_UP:
			ui.selUp();
			break;
		case KEY_DOWN:
			ui.selDown();
			break;
		case 10: // enter
		case KEY_RIGHT:
			enterSel(ui.enter());
			break;
		case KEY_LEFT:
			ui.back();
			break;
		case 27:  // esc
		case 113: // Q
			if (!ui.back()) {
				return;
			}
			break;
		default:
			std::cout << key;
			break;
		}
		/*if (!games.empty() && !games[ui.sel[0]].running && games[ui.sel[0]].thread && games[ui.sel[0]].thread->joinable()) {
			games[ui.sel[0]].thread->join();
			delete games[ui.sel[0]].thread;
			ui.refreshMain();
		}*/
	}
}

void NLaunch::enterSel(int item) {
	int game = findGame("currentItem");
	Game temp;

	switch (item) {
	case -1:
		return;
	case 0:
		launchconf.config();
		break;
	case 1:
		addGame();
		break;
	case 2:
		if (game > -1) {
			if (games.at(game).running) {
				stopGame(game);
			} else {
				// std::cout << "starting " << games[i].name.toStdString() << "\n";
				startGame(game);
			}
		}
		break;
	case 3:
		temp = games.at(game);
		if (gameconf.Config(&temp)) {
			games.at(game) = temp;
			checkdxvk(game);
			checkvkd3d(game);
		}
		break;
	case 4:
		winetools(game);
		break;
	}
	ui.touchMain();
}

void NLaunch::addGame() {
	Game game = gameconf.defaultconf;
	game.name = "";

	if (gameconf.Config(&game)) { // edit empty game
		games.emplace_back(game);

		if(!game.directory.empty()) {
			if (!fs::is_directory(game.directory)) {
				fs::create_directory(game.directory);
			}
			int gamenum = findGame(game.name);
			winerun(gamenum, "wineboot");
			checkdxvk(gamenum);
			checkvkd3d(gamenum);
		} else {
			ui.showMessage("Game directory not set", true);
		}
	}
// ui.refreshMain();
}
void NLaunch::winetools(int game) {
	std::string winetool;
	std::thread *thread;

	ui.selectFrom({"Winecfg", "Regedit", "Wine console", "Winetricks", "Run exe in wineprefix"}, &winetool);

	if (winetool == "Winecfg") thread = new std::thread(&NLaunch::winerun, this, game, "winecfg");
	else if (winetool == "Regedit") thread = new std::thread(&NLaunch::winerun, this, game, "regedit");
	else if (winetool == "Wine console") thread = new std::thread(&NLaunch::winerun, this, game, "wineconsole");
	else if (winetool == "Winetricks") thread = new std::thread(&NLaunch::winetricks, this, game);
	else if (winetool == "Run exe in wineprefix") wineexe(game);

	if (!games[game].running) {
		games[game].running = true;
	}
	if (!games[game].thread) {
		games[game].thread = thread;
	} else {
		thread->detach();
	}
}

void NLaunch::startGame(int game) {
	std::string args = "'" + games[game].exec + "' " + games[game].args;
	//	args.append(games[game].args);
	games[game].thread = new std::thread(&NLaunch::winerun, this, game, args);
	games[game].running = true;
	//games[game].thread->detach();
	//delete games[game].thread;

	// games[game].gamelog = "";

	// ui.refreshMain();

	// games[game].thread->detach();
	// delete games[game].thread;
	// games[game].process->start();
	/*QDateTime time;
	games[game].runtime = time.currentDateTime();
	*/
}
/*

void NLaunch::onGameStopped() {
	int game = findGame("currentItem");
	if(game > -1) {
		QDateTime time; //calculate playtime
		long secs = games[game].runtime.secsTo(time.currentDateTime());
		int hours = secs / 60 / 60;
		secs -= hours * 60 * 60;
		int min = secs / 60;
		secs -= min * 60;
		secs += games[game].playsec;
		while (secs >=60) {
			min++;
			secs -= 60;
		}
		games[game].playsec = secs;
		min += games[game].playmin;
		while (min >= 60) {
			hours++;
			min -= 60;
		}
		games[game].playmin = min;
		games[game].playhour += hours;
		games[game].runtime = time.currentDateTime(); //prevent time from getting added twice
		showPlayed(game);
		gameconf.savePlayed(&games);
	}
}

void NLaunch::showPlayed(int game) {
	std::string played;
	if (games[game].playhour > 0) {
		played = std::to_string(games[game].playhour) + " hours ";
	}
	played += std::to_string(games[game].playmin) + " minutes";
	ui->timePlayed->setText(std::string::fromStdString(played));
	std::cout << played;
}
*/
void NLaunch::stopGame(int game) {
	std::string cmd = "WINEPREFIX='" + games[game].winepfx + "' '";
	if (games[game].runner == "Wine") {
		cmd += gameconf.winefolder+"/"; // + games[game].runnerver+"/bin/wine wineboot -k";
		//winerun(game, "wineboot -k");
	} else if (games[game].runner == "Proton") {
		cmd += gameconf.protonfolder + "/"; // + games[game].runnerver + "/files/bin/wineserver' -k";
		//system(cmd.c_str());
	}
	cmd += games[game].runnerver+"/bin/wine' wineboot -k &>/dev/null";
	//std::cout << "running: " << cmd << "\n";
	system(cmd.c_str());
	games[game].running = false;
	if (games[game].thread && games[game].thread->joinable()) {
		games[game].thread->join();
		delete games[game].thread;
	}
	// ui.touchMain();
}

int NLaunch::findGame(std::string name) {
	if (name.empty())
		return -1;
	if (name == "currentItem")
		return ui.sel[0];
	for (size_t i = 0; i < games.size(); i++) {
		if (name == games[i].name) {
			return i;
		}
	}
	return -1;
}

void NLaunch::checkdxvk(int game) {
	if (games[game].runner == "Proton") return;

	vecstr dxvk_dlls = {"d3d10core", "d3d11", "d3d9", "dxgi"};
	vecstr dxvkpath = {gameconf.dxvkfolder + "/" + games[game].dxvk + "/x64/",
					   gameconf.dxvkfolder + "/" + games[game].dxvk + "/x32/"};

	if (games[game].dxvk == "none") {
		restoredlls(game, dxvk_dlls);
	} else {
		replacedlls(game, dxvk_dlls, dxvkpath);
	}
}
void NLaunch::checkvkd3d(int game) {
	if (games[game].runner == "Proton") return;

	vecstr vkd3d_dlls = {"d3d12core", "d3d12"};
	vecstr vkd3dpath = {gameconf.vkd3dfolder + "/" + games[game].vkd3d + "/x64/",
						gameconf.vkd3dfolder + "/" + games[game].vkd3d + "/x86/"};

	if (games[game].vkd3d == "none") {
		restoredlls(game, vkd3d_dlls);
	} else {
		replacedlls(game, vkd3d_dlls, vkd3dpath);
	}
}
void NLaunch::restoredlls(int game, vecstr dlls) {
	vecstr pfxpath = {games[game].winepfx + "/drive_c/windows/system32/",
					  games[game].winepfx + "/drive_c/windows/syswow64/"};

	for (size_t i=0; i<dlls.size(); i++) {
		bool restored = false;
		for (size_t j = 0; j < 2; j++) {
			fs::path cur_dll(pfxpath.at(j) + dlls.at(i) + ".dll");
			fs::path orig_dll(pfxpath.at(j) + dlls.at(i) + ".dll.orig");
			if (fs::exists(cur_dll) && fs::is_symlink(cur_dll)) {
				if(fs::remove(cur_dll)) {
					if (!fs::exists(orig_dll)) std::cout << "No " << orig_dll.filename() << " found to restore";
					else fs::rename(orig_dll, cur_dll);
					if (fs::exists(cur_dll)) restored = true;
					else std::cout << "Failed to restore " << dlls.at(i) << std::endl;
				} else {
					std::cout << "Failed to remove symlink " << dlls.at(i) << std::endl;
				}
			}
		}
		if (restored) {
			ui.showMessage("Removing dlloverride for " + dlls.at(i)+".dll", false);
			std::string cmd = "reg delete \"HKEY_CURRENT_USER\\Software\\Wine\\DllOverrides\" /v " + dlls.at(i) + " /f";
			//std::cout << "Running: " << cmd << std::endl;
			winerun(game, cmd);
		}
	}
}
void NLaunch::replacedlls(int game, vecstr dlls, vecstr dllpaths) {
	vecstr pfxpath = {games[game].winepfx + "/drive_c/windows/system32/",
					  games[game].winepfx + "/drive_c/windows/syswow64/"};
	std::string cmd;
	for (std::string file : dlls) {
		bool linked;
		for (int i = 0; i < 2; i++) {
			linked = symlinkdll(file, pfxpath.at(i), dllpaths.at(i));
		}
		if (linked) {
			ui.showMessage("Adding dlloverride for "+ file+".dll", false);
			cmd = "reg add \"HKEY_CURRENT_USER\\Software\\Wine\\DllOverrides\" /v " + fs::path(file).stem().generic_string() + " /d native /f";
			//std::cout << "Running: " << cmd << std::endl;
			winerun(game, cmd);
		}
	}
}
bool NLaunch::symlinkdll(std::string dll, std::string pfxpath, std::string dllpath) {
	fs::path cur_dll(pfxpath + dll + ".dll");
	fs::path orig_dll(pfxpath + dll + ".dll.orig");
	fs::path tolink(dllpath + dll + ".dll");

	if(!fs::exists(tolink)) {
		std::cerr << "File " + dll  +".dll does not exists in " + pfxpath << std::endl;
	}

	if (fs::exists(cur_dll) && fs::is_symlink(cur_dll)) {
		if (fs::read_symlink(cur_dll) == tolink) {
			return false;
		}
		fs::remove(cur_dll);
	} else {
		fs::rename(cur_dll, orig_dll);
		if (!fs::exists(orig_dll)) std::cerr << pfxpath + dll + " rename failed\n";
	}

	fs::create_symlink(tolink, cur_dll);
	if (!fs::is_symlink(cur_dll) || fs::read_symlink(cur_dll) != tolink) {
		std::cerr << dllpath + dll + " symlink failed\n";
		return false;
	}
	return true;
}

void NLaunch::winetricks(int game) {
	if(game != -1) {
		std::string cmd = "WINEPREFIX='"+games[game].winepfx;
		if(games[game].runner == "Wine")
			cmd += "' WINE='"+gameconf.winefolder+"/" + games[game].runnerver+"/bin/wine";
		else if(games[game].runner == "Proton")
			cmd += "' WINE='"+gameconf.protonfolder+"/" + games[game].runnerver+"/files/bin/wine";
		cmd += "' winetricks";
		system(cmd.c_str());

		if (games[game].thread->joinable()) {
			games[game].thread->join();
			games[game].running = false;
			delete games[game].thread;
		}
	}
}
void NLaunch::wineexe(int game) {
	std::thread *thread;
	if(game > -1) {
		std::string filename;
		ui.editString(&filename);
		if(!filename.empty()) {
			thread = new std::thread(&NLaunch::winerun, this, game, filename);

			if (!games[game].running) {
				games[game].running = true;
			}
			if (!games[game].thread) {
				games[game].thread = thread;
			} else {
				thread->detach();
				delete thread;
			}
		}
	}
}

void NLaunch::winerun(int game, std::string args) {
	std::string cmd, winecmd; // = games.at(game).exec +" "+ args;

	cmd = "cd '" + games[game].directory + "' && sh -c \"WINEPREFIX='" + games[game].winepfx + "'";
	//cmd = "PWD='" + games[game].directory + "' sh -c \"WINEPREFIX='" + games[game].winepfx + "'";
	if (games[game].runner == "Wine") {
		winecmd = gameconf.winefolder + "/" + games[game].runnerver + "/bin/wine";
	} else if (games[game].runner == "Proton") {
		cmd += " PROTONPATH='" + gameconf.protonfolder + "/" + games[game].runnerver;
		cmd += "' GAMEID=" + games[game].umu_id + " ";
		winecmd = "umu-run";
	}

	cmd += " " + games[game].env + " " + games[game].cmdpfx;
	cmd += " '" + winecmd + "' " + args;
	if (!games[game].log)
		cmd += " &>/dev/null\"";
	else
		cmd += " &>" + games[game].logpath + "\"";


	//std::cout << cmd << std::endl;
	int ret = system(cmd.c_str());
	/*if (ret != 0) {
		ui.showMessage("Error running command: "+cmd);
	}*/
	if (games[game].runner == "Wine") {
		cmd = "WINEPREFIX='" + games[game].winepfx + "' '" + winecmd + "server' -w &>/dev/null";
		system(cmd.c_str());
	}

	//if (games[game].thread->joinable()) {
	//	games[game].thread->join();
		games[game].running = false;
	//	delete games[game].thread;
	//}
	ui.refreshMain();
	// vecstr wineargs;
	/*if (games[game].runner == "Wine") {
		env.insert("WINEPATH", gameconf.winefolder+"/"+games[game].runnerver+"/bin/wine");
		//process->setWorkingDirectory(games[game].directory);
		process->setProgram(launchconf.confpath+"winerun");
	} else if (games[game].runner == "Proton") {
		//wineargs.append(gameconf.protonfolder+"/"+games[game].runnerver);
		env.insert("PROTONPATH", gameconf.protonfolder+"/"+games[game].runnerver);
		env.insert("GAMEID", games[game].ulwgl_id);
		process->setProgram(gameconf.ulwglfolder+"/ulwgl-run");
	}
	//wineargs.append(args);
	process->setProcessEnvironment(env);
	process->setWorkingDirectory(games[game].directory);
	process->setArguments(args);
	process->setProcessChannelMode(QProcess::ProcessChannelMode::MergedChannels);
	process->setReadChannel(QProcess::StandardOutput);
	process->start();

	if (process != games[game].process) {
		process->waitForFinished(-1);
		process->close();
		delete process;
	}*/
}
/*
pid_t NLaunch::runProcess(char **argv, char **envp) {
	pid_t pid;
	int status;

	if ((pid = fork()) == 0) {
		if ((execve(argv[0], argv, envp)) == -1) {
			ui.showMessage((std::string)"Failed to run process " + argv[0]);
		}
	}
	return pid;
}*/
