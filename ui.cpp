#include "ui.h"
#include "nlaunch.h"
#include <cstddef>
#include <ncurses.h>
#include <iostream>
#include <string>
#include <sys/types.h>
#include <vector>
#include <array>

UI::UI() {
	screen = initscr();
	if (has_colors() == FALSE) {
		endwin();
		std::cerr << "Your terminal does not support color\n";
		exit(1);
	}
	use_default_colors();
	start_color();
	init_pair(1, COLOR_BLACK, COLOR_WHITE);
	init_pair(2, COLOR_BLACK, 8);

	gamelist = newwin(screen->_maxy, screen->_maxx/2, 0, 0);
	box(gamelist, 0, 0);
	menuwin = newwin(screen->_maxy, screen->_maxx/2, 0, screen->_maxx/2);
	box(menuwin, 0, 0);
	confwin = newwin(screen->_maxy/2, screen->_maxx*0.7, screen->_maxy/4, screen->_maxx/6);
	box(confwin, 0, 0);
	noecho();
	ESCDELAY = 10;
	keypad(screen, TRUE);
	curs_set(0);
	refresh();
}
UI::~UI() {
	endwin();
}

void UI::selUp() {
	switch (current) {
	case GAMELIST:
		if (sel[0] > 0) {
			sel[0]--;
		} else {
			sel[0] = games->size()-1;
		}
		refreshMain();
		break;
	case MENU:
		if (sel[1] > 0) {
			sel[1]--;
		} else {
			sel[1] = menusize-1;
		}
		refreshMain();
		break;
	}
}
void UI::selDown() {
	switch (current) {
	case GAMELIST:
		if (sel[0] < games->size()-1) {
			sel[0]++;
		} else {
			sel[0] = 0;
		}
		refreshMain();
		break;
	case MENU:
		if (sel[1] < menusize-1) {
			sel[1]++;
		} else {
			sel[1] = 0;
		}
		refreshMain();
		break;
	}
}
bool UI::back() {
	if(current == MENU && !games->empty()) {
		current = GAMELIST;
		refreshMain();
		return true;
	}
	return false;
}
int UI::enter() {
	switch(current) {
	case GAMELIST:
		current = MENU;
		refreshMain();
		return -1;
	case MENU:
		return sel[1];
	}
}

WINDOW* UI::getConfWin() {
	return confwin;
}
void UI::touchMain() {
	touchwin(gamelist);
	touchwin(menuwin);
	refreshMain();
}

void UI::refreshMain() {
	int gamecolor, menucolor;
	switch (current) {
	case GAMELIST:
		gamecolor = COLOR_PAIR(1);
		menucolor = COLOR_PAIR(2);
		break;
	case MENU:
		gamecolor = COLOR_PAIR(2);
		menucolor = COLOR_PAIR(1);
		break;
	}
	//draw gamelist
	for (int i = 0; i < games->size(); i++) {
		if (i == sel[0]) { //highlight selected
			wattron(gamelist, gamecolor);
		} else wattroff(gamelist, gamecolor);
		mvwprintw(gamelist, 1+i, 1, " %s ", games->at(i).name.c_str());
	}
	wrefresh(gamelist);

	std::vector<std::string> menu_items = {
		"Launcher config",
		"Add game"
	};
	if(!games->empty()) {
		if(games->at(sel[0]).running) menu_items.emplace_back("Stop");
		else menu_items.emplace_back("Play");
		menu_items.emplace_back("Game config");
		menu_items.emplace_back("Wine tools");
		//if(games->at(sel[0]).log) menu_items.emplace_back("Game log");
	}
	menusize = menu_items.size();

	for (int i=0; i<menu_items.size(); i++) {
		if (i == sel[1]) wattron(menuwin, menucolor);
		else wattroff(menuwin, menucolor);
		mvwprintw(menuwin, 1+i, 1, " %s ", menu_items.at(i).c_str());
	}
	wrefresh(menuwin);
}

int UI::readKey() {
	return wgetch(screen);
}
void UI::editString(std::string *str) {
	WINDOW *strwin = newwin(3, screen->_maxx*0.7, screen->_maxy/2-1, screen->_maxx/6);
	box(strwin, 0, 0);
	//echo();
	//wechochar(strwin, const chtype)
	curs_set(1);

	std::string input;
	input.append(*str);
	mvwprintw(strwin, 1, 1, "%s", input.c_str());
	wrefresh(strwin);

	int key = readKey();
	size_t pos=input.size();
	while (key != 10 && key != 27) {
		switch(key) {
		case 263:
			if(!input.empty() && pos>0) {
				input.erase(--pos, 1);
			}
			break;
		case KEY_LEFT:
			if (pos>0) pos--;
			break;
		case KEY_RIGHT:
			if (pos < input.size()) pos++;
			break;
		case KEY_HOME:
			pos = 0;
			break;
		case KEY_END:
			pos = input.size();
			break;
		default:
			input.insert(pos, 1, key);
			pos++;
			//wechochar(strwin, key);
			break;
		}
		wclear(strwin);
		box(strwin, 0, 0);
		mvwprintw(strwin, 1, 1, "%s", input.c_str());
		//mvwprintw(strwin, 100, 1, "pos: %d", pos);
		wmove(strwin, 1, 1+pos);
		wrefresh(strwin);
		key=readKey();
	}
	if (key==10) {
		str->clear();
		str->append(input);
	}
	curs_set(0);
}

//Select 1 value from items into *str
void UI::selectFrom(std::vector<std::string> items, std::string *str) {
	WINDOW *selwin = newwin(items.size()+2, screen->_maxx*0.7, screen->_maxy/2-items.size()/2-1, screen->_maxx/6);
	box(selwin, 0, 0);

	int key;
	size_t sel=0;
	do {
		for (size_t i=0; i<items.size(); i++) {
			if(i==sel) wattron(selwin, COLOR_PAIR(1));
			else wattroff(selwin, COLOR_PAIR(1));
			mvwprintw(selwin, 1+i, 1, " %s ", items.at(i).c_str());
		}
		wrefresh(selwin);
		key = readKey();
		switch (key) {
		case KEY_UP:
			if (sel > 0) sel--;
			else sel=items.size()-1;
			break;
		case KEY_DOWN:
			if (sel < items.size()-1) sel++;
			else sel=0;
			break;
		case 10:
			*str = items.at(sel);
		}
	} while (key!=10 && key!=27);

	//werase(selwin);
	touchMain();
	return;

}

int UI::showMessage(std::string msg, bool getkey) {
	WINDOW *msgwin = newwin(5, screen->_maxx*0.7, screen->_maxy/2-3, screen->_maxx/6);
	box(msgwin, 0, 0);

	mvwprintw(msgwin, 2, msgwin->_maxx/2-msg.length()/2, "%s", msg.c_str());
	wrefresh(msgwin);

	if (getkey) {
		int key = readKey();
	/*while (key!=10 && key!=27) {
		key = readKey();
	}*/
	//touchMain();
		return key;
	}
	return 0;
}
