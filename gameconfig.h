#ifndef GAMECONFIG_H
#define GAMECONFIG_H

#include <string>
#include <thread>
#include <vector>
#include <ncurses.h>

#define CONFIG_PATH "/.local/share/ncurses-launcher/"

class Game {
public:
	explicit Game();
	std::string name;
	std::string directory;
	std::string exec;
	std::string winepfx;
	std::string runner;
	std::string runnerver;
	std::string umu_id;
	std::thread *thread;
	bool log;
	std::string logpath;
	std::string cmdpfx;
	std::string args;
	std::string dxvk;
	std::string vkd3d;
	std::string env;
	//QDateTime runtime;
	int playhour = 0, playmin = 0, playsec = 0;
	bool running;
};

class GameConfig
{

public:
	GameConfig();
	~GameConfig();

	void readRunnerDxvk(Game *confgame);
	bool Config(Game *confgame);
	void saveGame(Game *game);
	Game loadGame(std::string name);
	void loadGames(std::vector<Game> *games); //, std::vector<std::string> *gamelist);
	void savePlayed(std::vector<Game> *games);
	void loadPlayed(std::vector<Game> *games);

	Game *game;
	Game defaultconf;
	std::vector<std::string> winever;
	std::vector<std::string> protonver;
	std::vector<std::string> dxvkver;
	std::vector<std::string> vkd3dver;
	std::string winefolder;
	std::string protonfolder;
	std::string dxvkfolder;
	std::string vkd3dfolder;
	std::string ulwglfolder;

	class UI *ui;
/*
	void on_ulwglSearch_clicked();
*/
private:
	void selectRunner(Game *game);
	void readDir(std::string *path, std::vector<std::string> *dirs);
	std::string confpath;
};


#endif // GAMECONFIG_H
