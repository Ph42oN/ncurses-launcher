#include "launcherconfig.h"
#include "gameconfig.h"
#include "nlaunch.h"
#include "ui.h"
#include <fstream>
#include <iostream>
#include <ncurses.h>
#include <string>
#include <thread>
#include <filesystem>
#include <vector>

namespace fs = std::filesystem;

LauncherConfig::LauncherConfig()
{
	confpath = (std::string)getenv("HOME")+CONFIG_PATH;
}

LauncherConfig::~LauncherConfig()
{
}

void LauncherConfig::init() {
	loadConfig();

	if (!fs::is_directory(confpath)) {
		fs::create_directory(confpath);
		fs::create_directory(confpath+"games");
	}
}

void LauncherConfig::config() {
	WINDOW *confwin = launcher->ui.getConfWin();

	int key=0;
	size_t sel=0;
	bool changed=TRUE;
	std::vector<std::string> conf_items;

	while(1) {
		if(changed) {
			wclear(confwin);
			wattroff(confwin, COLOR_PAIR(1));
			box(confwin, 0, 0);
			conf_items.clear();
			conf_items.emplace_back("Wine path:   " + gameconf->winefolder);
			conf_items.emplace_back("Proton path: " + gameconf->protonfolder);
			conf_items.emplace_back("DXVK path:   " + gameconf->dxvkfolder);
			conf_items.emplace_back("VKD3D path:  " + gameconf->vkd3dfolder);
			//conf_items.emplace_back(" UMU launcher path:\t"
			conf_items.emplace_back("Default game config");
			conf_items.emplace_back("Save");
			changed=FALSE;
		}
		for (size_t i=0; i<conf_items.size(); i++) {
			if(i==sel) wattron(confwin, COLOR_PAIR(1));
			else wattroff(confwin, COLOR_PAIR(1));
			mvwprintw(confwin, 1+i, 1, " %s ", conf_items.at(i).c_str());
		}
		wrefresh(confwin);
		key=launcher->ui.readKey();
		switch (key) {
		case KEY_UP:
			if (sel > 0) sel--;
			else sel=conf_items.size()-1;
			break;
		case KEY_DOWN:
			if (sel < conf_items.size()-1) sel++;
			else sel=0;
			break;
		case 10:
			switch (sel) {
				case 0:	launcher->ui.editString(&gameconf->winefolder); break;
				case 1:	launcher->ui.editString(&gameconf->protonfolder); break;
				case 2:	launcher->ui.editString(&gameconf->dxvkfolder); break;
				case 3:	launcher->ui.editString(&gameconf->vkd3dfolder); break;
				case 4: gameconf->Config(&gameconf->defaultconf); break;
				case 5: saveConfig(); launcher->ui.touchMain(); return;
			}
			changed=TRUE;
			break;
		case 27: //esc
		case 113: //Q
			key = launcher->ui.showMessage("Go back without saving? (y/enter = yes)", true);
			if (key==10 || key==121) {
				launcher->ui.touchMain();
				return;
			}
			touchwin(confwin);
			break;
		}
	}
}

void LauncherConfig::loadConfig() {
	std::ifstream file(confpath+"launcher.conf");
	if(!file.good()) {
		//std::cout << "Using default wine and dxvk folders\n";
		gameconf->winefolder = confpath+"wine";
		gameconf->protonfolder = (std::string)getenv("HOME")+"/.local/share/Steam/compatibilitytools.d";
		gameconf->dxvkfolder = confpath+"dxvk";
		gameconf->vkd3dfolder = confpath+"vkd3d";
		gameconf->ulwglfolder = (std::string)getenv("HOME")+"/.local/share/ULWGL";
		return;
	}
	std::string buffer;
	while (getline(file, buffer)) {
		if(buffer.substr(0,10) == "winefolder") {
			gameconf->winefolder = buffer.substr(buffer.find('=')+1, buffer.npos).c_str();
		} else if(buffer.substr(0,12) == "protonfolder") {
			gameconf->protonfolder = buffer.substr(buffer.find('=')+1, buffer.npos).c_str();
		} else if(buffer.substr(0,10) == "dxvkfolder") {
			gameconf->dxvkfolder = buffer.substr(buffer.find('=')+1, buffer.npos).c_str();
		} else if(buffer.substr(0,11) == "vkd3dfolder") {
			gameconf->vkd3dfolder = buffer.substr(buffer.find('=')+1, buffer.npos).c_str();
		} else if(buffer.substr(0,11) == "ulwglfolder") {
			gameconf->ulwglfolder = buffer.substr(buffer.find('=')+1, buffer.npos).c_str();
		}
	}
	file.close();
}
/*
void LauncherConfig::downloadfile(char *url) {
	QDir dir(confpath+"download");
	if (!dir.exists()) {
		checkDir(confpath+"download");
	}
	runProcess("wget", {"-P", confpath + "download", url});
	//processDialog.setWindowTitle("downloading...");
	//return waitProcess();
}

void LauncherConfig::checkulwgl(QString ulwglfolder, QString confpath) {
	QFileInfo file(ulwglfolder + "/ulwgl-run");
	QDir dir(ulwglfolder);
	QString ulwgltar = confpath + "download/ULWGL-launcher.tar.gz";
	if (!file.exists() || !dir.exists()) {
		checkDir(ulwglfolder);
		file.setFile(ulwgltar);
		if(!file.exists()) {
			std::cout << "Downloading ULWGL\n";
			downloadfile("https://github.com/Open-Wine-Components/ULWGL-launcher/releases/download/0.1-RC3/ULWGL-launcher.tar.gz");
		}
		nextProcess = {"tar", {"-xvf", confpath + "download/ULWGL-launcher.tar.gz", "-C", ulwglfolder}};
	}
}*/

void LauncherConfig::saveConfig() {
	std::ofstream file(confpath+"launcher.conf");
	if(!file.good()) {
		//launcher->ui.showMessage("Error saving launcher.conf\n", true);
		return;
	}
	file << "winefolder=" << gameconf->winefolder << std::endl;
	file << "protonfolder=" << gameconf->protonfolder << std::endl;
	file << "dxvkfolder=" << gameconf->dxvkfolder << std::endl;
	file << "vkd3dfolder=" << gameconf->vkd3dfolder << std::endl;
	file << "ulwglfolder=" << gameconf->ulwglfolder << std::endl;
	file.close();
	//}
	checkDir(gameconf->winefolder);
	checkDir(gameconf->protonfolder);
	checkDir(gameconf->dxvkfolder);
	checkDir(gameconf->vkd3dfolder);

	//checkulwgl(gameconf->ulwglfolder, confpath);
}

void LauncherConfig::checkDir(std::string path) {
	if (path.empty()) {
		std::cerr << "path is empty\n";
		return;
	}
	if (!fs::is_directory(path)) {
		fs::create_directory(path);
	}
}
