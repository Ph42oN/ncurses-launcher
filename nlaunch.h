#ifndef NLAUNCH_H
#define NLAUNCH_H

#include "gameconfig.h"
#include "launcherconfig.h"
#include "ui.h"
#include <ncurses.h>
#include <string>
#include <vector>

typedef std::vector<std::string> vecstr;

class NLaunch {
public:
    NLaunch();
    ~NLaunch();

	void run();

	std::vector<Game> games;
    // std::vector<std::string> gamelist;
    int selected=0;
    LauncherConfig launchconf;
    GameConfig gameconf;
    class UI ui;

private:

	void enterSel(int item);
    void addGame();
    void onGameStopped();
    void showPlayed(int game);
    void startGame(int game);
    void stopGame(int game);
    bool gameRunning(int game);
    int findGame(std::string name);
    void gameLog();
    void gameLogErr();
    void checkdxvk(int game);
    void checkvkd3d(int game);
    void restoredlls(int game, vecstr dlls);
    void replacedlls(int game, vecstr dlls, vecstr dllpaths);
    bool symlinkdll(std::string dll, std::string pfxpath, std::string dllpath);

    void winetools(int game);
    void wineexe(int game);
    void winetricks(int game);
    void winerun(int game, std::string args);
    //pid_t runProcess(char **argv, char **envp);

/*
    void on_gameList_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);

    void on_settingButton_clicked();
    void on_logButton_clicked();
    void on_wineButton_clicked();

private:
    Ui::QtLaunch *ui;
    QMessageBox message;
    QDialog logDialog;
    Ui::GameLog logUi;*/
};

#endif // NLAUNCH_H
